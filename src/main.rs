use csv::Writer;
use num_bigint::BigUint;
use num_traits::{One, Zero};
use structopt::StructOpt;

// Copyright (C) 2021 S. Gay

#[derive(StructOpt)]
#[structopt(about = "Calculate n-many Fibonnaci numbers.")]
struct Cli {
    #[structopt(
        short = "n",
        long = "max_index",
        default_value = "100",
        help = "The n-th Fib. number to calculate. Note that returned values are 0-indexed",
        verbatim_doc_comment
    )]
    max_ind: i32,
    #[structopt(
        short = "c",
        long = "csv",
        help = "Writes all calculated numbers to 'fib.csv' (appends if existing)"
    )]
    to_csv: bool,
    #[structopt(
        short = "v",
        long = "verbose",
        help = "Prints out each number as it's calculated"
    )]
    verbose: bool,
}

fn write_csv_vec(
    indices: &Vec<String>,
    values: &Vec<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let file = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open("fib.csv")
        .unwrap();
    let mut wtr = Writer::from_writer(file);
    for zipped in indices.iter().zip(values) {
        wtr.write_record(&[zipped.0, zipped.1])?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    let args = Cli::from_args();
    let final_fib = args.max_ind;

    let vec_size: usize = 10_000;

    let mut indices: Vec<String> = Vec::with_capacity(vec_size);
    let mut fibs: Vec<String> = Vec::with_capacity(vec_size);

    // Set up initial conditions
    let mut f: BigUint;
    let mut f_0: BigUint = Zero::zero();
    let mut f_1: BigUint = One::one();

    if final_fib >= 0 {
        if args.verbose {
            println!("{},{}", 0, f_0)
        }
        indices.push(0.to_string());
        fibs.push(f_0.to_string());
    }
    if final_fib >= 1 {
        if args.verbose {
            println!("{},{}", 1, f_1)
        }
        indices.push(1.to_string());
        fibs.push(f_1.to_string());
    }

    let write_every = vec_size as i32;
    // Calculate subsequent Fibonnaci numbers
    for i in 2..final_fib {
        f = f_0 + &f_1;
        indices.push(i.to_string());
        fibs.push(f.to_string());
        if args.verbose {
            println!("{},{}", i, f);
        }

        let rem = (i + 1) % write_every;
        if args.to_csv {
            if rem == 0 {
                write_csv_vec(&indices, &fibs);
            }
        }
        f_0 = f_1;
        f_1 = f;

        //  Reset to empty to avoid
        //  (a) exceeding capacity and needing expensive re-allocations
        //  (b) writing duplicates to the .csv (if applicable)
        if rem == 0 {
            indices = Vec::with_capacity(vec_size);
            fibs = Vec::with_capacity(vec_size);
        }
    }
    if args.to_csv {
        write_csv_vec(&indices, &fibs);
    }
}
